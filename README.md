#SquaredS - Stock, Sentiment - **Dash**board

---
## Todo
1. Improve styling
2. Add proper documentation (I've commented the code for the time being)
3. Improve visualisation of sentiments
4. **Knowledge Graph**

---

## Files

- app.py -> the main application
- static folder -> contains CSS styling
- tickers.csv -> data for company tickers and company names
- Watson.py -> watson class for tone analysis

---

## Basic flow

1. The stock data and news data are fetched ONCE, whenver the user changes the inputs
2. The data is then stored onto intermediate "div" elements, where it can be used by different graphs and components

---

##Installation and run
1. Install requirements via "pip install -r requirements.txt"
2. Run the server by "python app.py"

