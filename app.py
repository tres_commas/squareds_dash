#Dash app
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go

#Data Readers
import pandas as pd
from pandas_datareader.data import DataReader

#Trends
from pytrends.request import TrendReq

#NLP
from Watson import Watson
from textstat.textstat import textstat

#News Requirements
import datetime as dt
import json
import requests
import re

app = dash.Dash(__name__)
server = app.server

#Reading the list of all NASDAQ companies
tickers_list = pd.read_csv('tickers.csv')

#Loading the visual elements onto the web-page
app.layout = html.Div(children=[
    html.Div(children=[
        html.Link(
            rel='stylesheet',
            href='/static/stylesheet.css'
        ),
        html.H2(children='Squared-S |'),
        html.Div([
            dcc.Dropdown(
            id='stock-ticker-input',
            options=[{'label': s[0], 'value': str(s[1])} for s in zip(tickers_list.Company, tickers_list.Symbol)],
            value='AAPL',
            multi=False
         ),
            dcc.Input(id='news-input', value='apple, iphone', type='text',placeholder='News phrase to search',style={'display': 'block'}),
            dcc.DatePickerRange(
                    id='date-picker-range',
                    start_date=dt.datetime(2018, 5, 16),
                    end_date=dt.datetime.now()
            )
        ]),
        html.Div(id='intermediate-daynews',style={'display': 'none'}),
        html.Div(id='intermediate-allnews',style={'display': 'none'}),
        html.Div(id='intermediate-df',style={'display': 'none'})
    ], className="container"),
    html.Div(id='graph-container',style={'paddingTop': '30'}),
    html.Div(id='trends-container',style={'paddingTop': '30'}),
    html.Div(children=[

        html.Div(className='row', children=[
            html.Div([
                html.H3("News | "),
            ]),
            html.Div(id='click-data'),
        ]),
        html.Div(id='sentiment-graph-container')
    ], className="container")
])

#--------Helper Functions------------------------------

#Returns a dictonary, maps one date to one description -> {date: description,date: description,...}
def json_to_dict(df,articles):
    desciption_dict = dict()
    pattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    for date in df.Date:
        stock_date = re.match(pattern, str(date))
        for article in articles:
            pub_date = re.match(pattern, article["publishedAt"])
            if(pub_date[0] == stock_date[0]):
                temp = stock_date[0]
                desciption_dict[temp] = article["description"]
    return desciption_dict

#For a given {date: description} create an list of annotations to be placed in the stock-graph
def create_annotation_list(df,desc_dict):
    temp = [] #array that will store the annoations created
    pattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    for i in range(1,len(df.Date)): #loop through all the dates from the stock data
        if((abs(df.Close[i-1]-df.Close[i]))/df.Close[i-1] * 100 >= 1): #annotate only if the percentage change is greater than 1%
            parsed_date = re.match(pattern, str(df.Date[i]))
            parsed_date = parsed_date[0]
            try:
                temp_dict = dict(
                        x=df.Date[i],
                        y=df.Close[i],
                        xref='x',
                        yref='y',
                        text=desc_dict[parsed_date],
                        showarrow=True,
                        arrowhead=2,
                        arrowcolor = "white",
                        arrowwidth=1.5,
                        font=dict(
                            size=11,
                            color="#DADADA"
                        ),
                        ax=0,
                        ay=-40
                    )
                temp.append(temp_dict)
            except:
                pass
    return temp

#--------Get stock data and post to intermediate-------
@app.callback(
    Output('intermediate-df','children'),
    [Input('stock-ticker-input', 'value'),Input('date-picker-range', 'start_date'),Input('date-picker-range', 'end_date')])
def update_stocks_df(ticker,start_date,end_date):
    try:
        df = DataReader(str(ticker), 'morningstar', start_date,end_date).reset_index()
    except:
        return(html.H3('Data is not available for {}'.format(ticker),style={'marginTop': 20, 'marginBottom': 20}))
    return df.to_json(date_format='iso',orient='split')

#--------Get news articles and post to intermediate-------
@app.callback(Output('intermediate-allnews', 'children'), [Input('news-input', 'value'),Input('date-picker-range', 'start_date'),Input('date-picker-range', 'end_date')])
def get_news(keyword, start_date, end_date):
    if(keyword == ''):
        return json.dumps('')

    keywords = [x.strip() for x in keyword.split(',')]  #Extract keywords from the keyword inputs, "apple, iphone" -> ["apple","iphone"]
    news_list = [] #Temporary holder to store the articles from each keyword

    #Regular expressions to parse the dates
    pattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    start = re.match(pattern, start_date)
    start = start[0]

    end = re.match(pattern, end_date)
    end = end[0]

    for word in keywords:
        news_response = requests.get('https://newsapi.org/v2/everything?q='+word+'&from='+start+'&to='+end+'&domains=theverge.com,nytimes.com,bbc.co.uk&sortBy=relevancy&pageSize=100&apiKey=4181b24ea18e4b62b0a2545b21030cda')
        json_data = json.loads(news_response.text)
        news_list.append(json_data["articles"])

    temp = [] #Temporary holder to store the COMBINED articles
    for articles in news_list:
        temp = temp + articles  #combine all articles onto a list

    cleanlist = []
    [cleanlist.append(x) for x in temp if x not in cleanlist] #Remove duplicates, if any
    return json.dumps(cleanlist)

#-----------------Stock-graph Section--------------
@app.callback(
    Output('graph-container','children'),
    [Input('intermediate-allnews', 'children'),Input('intermediate-df','children')])
def update_ticker_graph(news, stocks_df):
    df = pd.read_json(stocks_df, orient='split')
    news = json.loads(news)
    print(news)
    if(news == ''): #If no keywords are present, no need for annotations
        trace = go.Scatter(
            x = df.Date,
            y = df.Close
        )
        return(dcc.Graph(
                        id="ticker-graph",
                        figure={
                            'data': [trace],
                            'layout': go.Layout(
                                showlegend=False,
                                autosize=True,
                                title=str(df["Symbol"][0]), yaxis={'title':'Price ($USD)'},
                                plot_bgcolor='#181818',
                                paper_bgcolor='transparent',
                                font= {
                                    'color': '#FCFFF5'
                                },
                            )
                        }
                ))
    else:
        news_dict = json_to_dict(df, news)
        annotation_list = create_annotation_list(df, news_dict)
        trace = go.Scatter(
            x = df.Date,
            y = df.Close
        )
        return(dcc.Graph(
                        id="ticker-graph",
                        figure={
                            'data': [trace],
                            'layout': go.Layout(
                                showlegend=False,
                                autosize=True,
                                title=str(df["Symbol"][0]), yaxis={'title':'Price ($USD)'},
                                plot_bgcolor='#181818',
                                paper_bgcolor='transparent',
                                font= {
                                    'color': '#FCFFF5'
                                },
                                annotations=annotation_list
                            )
                        }
                ))

#-------------------News Section--------------------------------
app.config['suppress_callback_exceptions']=True #requirement to modify dynamically added elements
#Show news for a particular date, when the user selects a point on the stock-graph
@app.callback(
    Output('click-data', 'children'),
    [Input('intermediate-allnews', 'children'),Input('ticker-graph', 'clickData')])
def display_click_data(news_input, clickData):
    all_articles = json.loads(news_input)
    if(all_articles == ""):
        return
    if(clickData == None):
        return html.P("No articles")
    res = [] #returns the <p> elements containing news articles
    clicked_date = clickData['points'][0]['x']

    res.append(html.H6(clicked_date))
    pattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    for article in all_articles:
        pub_date = re.match(pattern, article["publishedAt"])
        if(pub_date[0] == clicked_date):
            if(article["description"] != ""):
                res.append(html.P(article["description"] + " - (" +str(textstat.flesch_kincaid_grade(article["description"])) + ")" )) #append a p-element containing description along with the readability-score
                res.append(html.Br())
    if(len(res) == 0):
        res.append(html.P("Nothing here."))
    return res

#-------------------Google Trends Section--------------------------------
@app.callback(
    Output('trends-container', 'children'),
    [Input('news-input', 'value'),Input('date-picker-range', 'start_date'),Input('date-picker-range', 'end_date')])
def display_trends(keyword, start_date, end_date):
    keywords = [x.strip() for x in keyword.split(',')] #splitting comma seperated words

    #Parsing the dates -> YYYY/MM/DD
    pattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    start = re.match(pattern, start_date)
    start = start[0]
    end = re.match(pattern, end_date)
    end = end[0]

    time_range = start+" "+ end #"2018-06-01 2018-06-12"

    #Fetch data from the API
    pytrends = TrendReq(hl='en-US', tz=360)
    kw_list = keywords
    pytrends.build_payload(kw_list, cat=0, timeframe=time_range, geo='', gprop='')

    df = pytrends.interest_over_time() #Returned data is a dataframe

    trends_plots =[] #stores all the different lines
    for word in keywords:
        r = go.Scatter(
            x = df.index,
            y=df[word],
            mode='lines',
            name = word
        )
        trends_plots.append(r)


    return(dcc.Graph(
                    id="ticker-trends-graph",
                    figure={
                            'data': trends_plots,
                            'layout': go.Layout(
                                showlegend=True,
                                autosize=True,
                                xaxis={'title':'Date'}, title="Interest",
                                plot_bgcolor='#030C12',
                                paper_bgcolor='transparent',
                                font= {
                                    'color': '#FCFFF5'
                                },
                                height=320
                            )
                        }
                )
        )
    return trends_graph


#-------------------News-sentiment Section--------------------------------

app.config['suppress_callback_exceptions']=True #requirement to modify dynamically added elements

#Returns list of the news OF A PARTICULAR DAY to the daynews intermediate -> carry out sentiment analysis for the day
@app.callback(
    Output('intermediate-daynews', 'children'),
    [Input('intermediate-allnews', 'children'),Input('ticker-graph', 'clickData')])
def push_daynews(news_input, clickData):
    all_articles = json.loads(news_input)
    if(all_articles == "" or clickData == None):
        return
    res = [] # returns the <p> elements containing news articles
    clicked_date = clickData['points'][0]['x']

    res.append(html.H6(clicked_date))
    pattern = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
    for article in all_articles:
        pub_date = re.match(pattern, article["publishedAt"])
        if(pub_date[0] == clicked_date):
            if(article["description"] != ""):
                res.append(article["description"])
    return res

#Graph the piechart of sentiments
@app.callback(
    Output('sentiment-graph-container', 'children'),
    [Input('intermediate-daynews', 'children')])
def sentiment_news(news):
    if(news == None):
        return
    watson = Watson()
    arr = news[1:] #removes unwanted "properties" data
    anger,fear,joy,sadness,analytical,confident,tentative = watson.analyze_text(arr)

    labels = ['Anger', 'Fear', 'Joy','Sadness','Analytical','Confident','Tentative']
    values = [anger,fear,joy,sadness,analytical,confident,tentative]
    colors = ['#8DD04A', '#962D3E', '#046380']
    trace_pie = go.Pie(labels=labels, values=values)

    return(dcc.Graph(
                    id="sentiment-pie-chart",
                    figure={
                        'data': [trace_pie],
                        'layout': go.Layout(
                            plot_bgcolor='#030C12',
                            paper_bgcolor='transparent',
                            font= {
                                'color': '#FCFFF5'
                            }
                        )
                    }
            )
    )

#-------------------End of Sections--------------------------------


external_css = ["https://fonts.googleapis.com/css?family=Product+Sans:400,400i,700,700i",
                "https://codepen.io/chriddyp/pen/bWLwgP.css"]

for css in external_css:
    app.css.append_css({"external_url": css})


if __name__ == '__main__':
    app.run_server(debug=True)
