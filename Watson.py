from watson_developer_cloud import ToneAnalyzerV3

class Watson:
    def __init__(self):
        self.tone_analyzer = ToneAnalyzerV3(
            version='2017-09-21',
            username='72ed67cb-1699-4d41-b067-cbaa5cbca046',
            password='KJYZIJFOG46H',
            url='https://gateway.watsonplatform.net/tone-analyzer/api'
        )
    def analyze_text(self,text_arr):
        content_type = 'application/json'
        temp = []
        anger = 0
        fear = 0
        joy = 0
        sadness = 0
        analytical = 0
        confident = 0
        tentative = 0

        for text in text_arr:
            tone = self.tone_analyzer.tone({"text":text}, content_type)
            temp.append(tone)

        moods = ["Anger","Fear","Joy","Sadness","Analytical","Confident","Tentative"]
        for tone in temp:
            try:
                text = tone['document_tone']['tones'][0]['tone_name']
                if(text == "Anger"):
                    anger+=1
                elif(text == "Fear"):
                    fear+=1
                elif(text == "Joy"):
                    joy+=1
                elif(text == "Sadness"):
                    sadness+=1
                elif(text == "Analytical"):
                    analytical+=1
                elif(text == "Confident"):
                    confident+=1
                elif(text == "Tentative"):
                    tentative+=1
            except:
                pass
        return anger,fear,joy,sadness,analytical,confident,tentative
